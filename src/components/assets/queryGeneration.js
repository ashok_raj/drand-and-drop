import { sqlframes } from '@sqlframes/repl-app/react';

import '@sqlframes/repl-app/styles';

const { DataFrame, View, SQL } = sqlframes;
const {
	groupBy, rollup, partitionBy, orderBy,
	where: { feq, eq, and, gte, gt, lte, lt, or, isin },
	agg: { sum, count, rank, ranko, avg }
} = SQL;


const queryGeneration = (jsonArrData, reportDef) => {
	let dfInput = DataFrame.fromJSONArray(jsonArrData);

	let finalDf = dfInput;

	// 1) FDF  using Page, and dimension values
	let arrFilters = [];
	if (reportDef.reportParameters.length > 0) {
		for (let i = 0; i < reportDef.reportParameters.length; i++) {
			let filter;
			if (reportDef.reportParameters[i].dimensionName == "Measures") {
				continue;
			}
			if (reportDef.reportParameters[i].dimensionValues.length > 0) {
				filter = isin(reportDef.reportParameters[i].dimensionName, reportDef.reportParameters[i].dimensionValues);
			}
			if (filter != null) {
				arrFilters.push(filter);
			}
		}
	}

	const filteredFinaldf = finalDf.fdf(
		and(
			...arrFilters
		),
	);

	// 2) Measures --> Expr fields, Agg fields, rank fields
	let dynamicAggAndCalcFields = [];
	if (reportDef.calculatedMeasures.length > 0) {
		for (let i = 0; i < reportDef.calculatedMeasures.length; i++) {
			let aggOrCalcField;
			//expr 
			if (reportDef.calculatedMeasures[i].func.length <= 0) {
				aggOrCalcField = SQL.script(reportDef.calculatedMeasures[i].expr).as(reportDef.calculatedMeasures[i].as);
			} else {	//agg
				switch (reportDef.calculatedMeasures[i].func) {
					case "sum":
						//sum func is used with an expr not with a direct col. Eg; won count + lost count sum
						if (reportDef.calculatedMeasures[i].col == "") {
							let sqlScript = SQL.script(reportDef.calculatedMeasures[i].expr);
							aggOrCalcField = sum(sqlScript).as(reportDef.calculatedMeasures[i].as);
						} else {
							aggOrCalcField = sum(reportDef.calculatedMeasures[i].col).as(reportDef.calculatedMeasures[i].as);
						}
						break;
					case "ranko":
						//ranko(partitionBy('Region'), orderBy(['Total CTC','DESC'])).as('Rank within Region'),
						if (reportDef.calculatedMeasures[i].partitionBy.length <= 0) {
							aggOrCalcField = ranko(0, orderBy(
								[reportDef.calculatedMeasures[i].orderBy.field, reportDef.calculatedMeasures[i].orderBy.order]
							)).as(reportDef.calculatedMeasures[i].as);
						} else {
							aggOrCalcField = ranko(partitionBy(...reportDef.calculatedMeasures[i].partitionBy),
								orderBy([reportDef.calculatedMeasures[i].orderBy.field, reportDef.calculatedMeasures[i].orderBy.order]
								)).as(reportDef.calculatedMeasures[i].as);
						}
						break;
					case "lead":
						aggOrCalcField = lead(reportDef.calculatedMeasures[i].col,
							reportDef.calculatedMeasures[i].offset,
							reportDef.calculatedMeasures[i].defaultValue,
							partitionBy(...reportDef.calculatedMeasures[i].partitionBy),
							orderBy(...reportDef.calculatedMeasures[i].orderBy.field)
						).as(reportDef.calculatedMeasures[i].as);
						break;
				}
			}
			if (aggOrCalcField != null) {
				dynamicAggAndCalcFields.push(aggOrCalcField);
			}
		}
	}

	// 3) GDF
	let groupByRowAxisArr = [];
	let groupByColAxisArr = [];
	let rollupsRowAxisArr = [];
	let rollupsColAxisArr = [];
	for (let i = 0; i < reportDef.reportParameters.length; i++) {
		if (reportDef.reportParameters[i].dimensionName != "Measures") {
			if (reportDef.reportParameters[i].axis == "row") {
				if (reportDef.reportParameters[i].groupby == true && reportDef.reportParameters[i].rollups == false) {
					groupByRowAxisArr.push(reportDef.reportParameters[i].dimensionName);
				} else if (reportDef.reportParameters[i].groupby == true && reportDef.reportParameters[i].rollups == true) {
					rollupsRowAxisArr.push(reportDef.reportParameters[i].dimensionName);
				}
			} else if (reportDef.reportParameters[i].axis == "column") {
				if (reportDef.reportParameters[i].groupby == true && reportDef.reportParameters[i].rollups == false) {
					groupByColAxisArr.push(reportDef.reportParameters[i].dimensionName);
				} else if (reportDef.reportParameters[i].groupby == true && reportDef.reportParameters[i].rollups == true) {
					rollupsColAxisArr.push(reportDef.reportParameters[i].dimensionName);
				}
			}
		}
	}
	let finalGdf;
	if (reportDef.isGDF) {
		// GDF
		finalGdf = filteredFinaldf.gdf(
			groupBy(
				//...reportDef.grouping.groupBy.cols,    // can also be groupby([cols], [cols], [rollups], [rollups])
				...groupByRowAxisArr, ...groupByColAxisArr,
				rollup(...rollupsRowAxisArr), rollup(...rollupsColAxisArr)
			), //make dynamic for any no of rollups and any no of combination of cols
			...dynamicAggAndCalcFields,         //agg
		)
	}


	// 4) pdf
	let pdfCols = [];
	for (let i = 0; i < reportDef.reportParameters.length; i++) {
		if (reportDef.reportParameters[i].dimensionName == "Measures") {
			pdfCols = reportDef.reportParameters[i].dimensionValues;
		}
	}
	let finalPdf = finalGdf.pdf(...pdfCols);

	finalPdf.fmt.addRowStyler(finalPdf.fmt.const({ color: 'darkred', 'background-color': 'red' }));

	// 5) VDF
	// rows axis and cols axis 
	let finalVdf = finalPdf;
	// if(reportDef.isVDF == true) {
	finalVdf = finalPdf.vdf(rollupsRowAxisArr, rollupsColAxisArr);
	// }

	// 6) HDF
	let finalHdf = finalVdf;
	if (reportDef.isHDF == true) {
		finalHdf = finalVdf.hdf(); //finalPdf.hdf(...rollupsArr);
		finalHdf.singleHierarchyField = false
		finalHdf.expandLevels = 10;
	}
	// 7) Formatting...
	for (let i = 0; i < reportDef.calculatedMeasures.length; i++) {
		if (reportDef.calculatedMeasures[i].format == null) {
			continue;
		}
		switch (reportDef.calculatedMeasures[i].format.type) {
			case "NumberFormat":
				let nbrFormatter = new Intl.NumberFormat(reportDef.calculatedMeasures[i].format.locale, reportDef.calculatedMeasures[i].format.options);
				finalHdf.fmt.setFieldFormatter(reportDef.calculatedMeasures[i].as, nbrFormatter);
				break;
		}
	}

	// 8) Render
	View.render(document.getElementById('DynamicReport'), finalHdf);
}

export default queryGeneration;