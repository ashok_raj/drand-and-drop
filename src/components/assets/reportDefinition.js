export const reportDefinition = {
    "reportParameters": [
        {
            "axis": "Page",
            "dimensionName": "Base Location",
            "dimensionValues": ["United States"]
        },
        {
            "axis": "Row",
            "dimensionName": "Department",
            "dimensionValues": ['US - Central', 'US - West'],
            "rollups": true,
            "groupby": true
        },
        {
            "axis": "Row",
            "dimensionName": "Product",
            "dimensionValues": [],
            "rollups": true,
            "groupby": true
        },
        {
            "axis": "Row",
            "dimensionName": "Employee Name",
            "dimensionValues": [],
            "rollups": true,
            "groupby": true
        },
        {
            "axis": "Column",
            "dimensionName": "Measures",
            "dimensionValues": ['Over All Rank', 'CalcScore', 'SRCost vs SalesAchieved', 'Sales Achivement', 'AvgOrdVal-WonOnly']
        }
    ],
    "calculatedMeasures": [
        {
            "index": 1,
            "func": "sum",
            "col": "Total Actual CTC",
            "as": "SRCost"
        },
        {
            "index": 2,
            "func": "sum",
            "col": "Actual Sales",
            "as": "SalesAchieved"
        },
        {
            "index": 3,
            "func": "sum",
            "col": "Sales Target $",
            "as": "SalesTarget"
        },
        {
            "index": 4,
            "func": "sum",
            "col": "Lost Count",
            "as": "LostCount"
        },
        {
            "index": 4,
            "func": "sum",
            "col": "Won Count",
            "as": "WonCount"
        },
        {
            "index": 5,
            "func": "sum",
            "col": "Days to Close",
            "as": "DC"
        },
        {
            "index": 6,
            "func": "sum",
            "col": "",
            "expr": "([Won Count]+[Lost Count])",
            "as": "DealCount"
        },
        {
            "index": 7,
            "func": "",
            "col": "",
            "expr": '([WonCount]/[DealCount])*100',
            "as": "DealConversionRate"
        },
        {
            "index": 8,
            "func": "",
            "col": "",
            "expr": '([SalesAchieved]/[WonCount])',
            "as": "AvgOrdVal-WonOnly",
            "format": {
                "type": "NumberFormat",
                "locale": "en-US",
                "options": {
                    "maximumFractionDigits": 2
                }
            }
        },
        {
            "index": 9,
            "func": "",
            "col": "",
            "expr": '([DC]/([DealCount]))',
            "as": "AverageTimeToClose(days)"
        },
        {
            "index": 10,
            "func": "",
            "col": "",
            "expr": '([SRCost]/[SalesAchieved])*100',
            "as": "SRCost vs SalesAchieved",
            "format": {
                "type": "NumberFormat",
                "locale": "en-US",
                "options": {
                    "style": "percent",
                    "minimumFractionDigits": 1
                }
            }
        },
        {
            "index": 11,
            "func": "",
            "col": "",
            "expr": '([SalesAchieved]/[SalesTarget])*100',
            "as": "Sales Achivement",
            "format": {
                "type": "NumberFormat",
                "locale": "en-US",
                "options": {
                    "maximumFractionDigits": 2
                }
            }
        },
        {
            "index": 12,
            "func": "ranko",
            "partitionBy": [],
            "orderBy": {
                "field": "AverageTimeToClose(days)",
                "order": "ASC"
            },
            "as": "RankTimeToCloseDays"
        },
        {
            "index": 13,
            "func": "",
            "col": "",
            "expr": '([SRCost vs SalesAchieved]*0.15)+([Sales Achivement]*0.5)+([AvgOrdVal-WonOnly]*0.1)+([DealConversionRate]*0.1)+([AverageTimeToClose(days)]*0.15)',
            "as": "CalcScore",
            "format": {
                "type": "NumberFormat",
                "locale": "en-US",
                "options": {
                    "maximumFractionDigits": 2
                }
            }
        },
        {
            "index": 14,
            "func": "ranko",
            "partitionBy": [],
            "orderBy": {
                "field": "CalcScore",
                "order": "DESC"
            },
            "as": "Over All Rank"
        },
        {
            "index": 15,
            "func": "ranko",
            "partitionBy": [],
            "orderBy": {
                "field": "SRCost vs SalesAchieved",
                "order": "DESC"
            },
            "as": "Cal_Rank_E"
        },
        {
            "index": 16,
            "func": "ranko",
            "partitionBy": ["Department"],
            "orderBy": {
                "field": "CalcScore",
                "order": "DESC"
            },
            "as": "Rank within Region"
        }
    ],
    "isGDF": true,
    "isHDF": true,
}

