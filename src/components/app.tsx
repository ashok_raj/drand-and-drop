// @ts-nocheck


import React, { Component, useRef, useEffect, Suspense } from "react";

//================================================ SQL FRAMES ================================
// Destructuring assignment of the exported namespaces 
import { sqlframes, repl, repl_react } from '@sqlframes/repl-app/react';
// to ensure the css is also bundled into the application css bundle
import '@sqlframes/repl-app/styles';
// import Main from './Main';
// import SpreadSheet from "./SpreadSheet";

// All the top level classNamees and objects exported in the namespaces
const { DataFrame, SQL, Time, View } = sqlframes;
const { REPL, REPLView } = repl;
const ReactREPL: any = repl_react.ReactREPL;

// for autosuggest of the SQL Frames API within the REPL editor
REPLView.addExtraLibs('/api/api.d.ts', 'api.d.ts');
//================================================ SQL FRAMES ================================
import { Routes, Route, BrowserRouter, Switch } from "react-router-dom";
import NavBar from "./components/NavBar";
// import TUIGrid from "./TUIGrid";
// import CollapseMenu from "./pages/CollapseMenu/CollapseMenu";
// import SIPCalculator from "./pages/SIPCalculator/SIPCalculator";
// import AnalysisEngine from "./pages/AnalysisEngine/AnalysisEngine";

import { Provider } from "react-redux";

import { createStore } from "redux";

import AppReducer from "./store/reducer/AppReducer";

const Main = React.lazy(() => import('./Main'));
const TUIGrid = React.lazy(() => import('./TUIGrid'));
const SpreadSheet = React.lazy(() => import('./SpreadSheet'));
const CollapseMenu = React.lazy(() => import('./pages/CollapseMenu/CollapseMenu'));
const SIPCalculator = React.lazy(() => import('./pages/SIPCalculator/SIPCalculator'));
const AnalysisEngine = React.lazy(() => import('./pages/AnalysisEngine/AnalysisEngine'));

const getFallBackUI = (comp) => {
	return <Suspense fallback={<></>}>
		{comp}
	</Suspense>
}



const store = createStore(AppReducer);



class App extends Component {
	render() {
		return (
			<div className="App mt-3  container">
				<Provider store={store}>
					<BrowserRouter >
						<NavBar />

						<Routes>
							<Route path="/" element={getFallBackUI(<AnalysisEngine />)} />
							<Route path="/sip-calculator" element={getFallBackUI(<SIPCalculator />)} />
							<Route path="/collapse-menu" element={getFallBackUI(<CollapseMenu />)} />
							<Route path="/main" element={getFallBackUI(<Main />)} />
							<Route path="spreadjs" element={getFallBackUI(<SpreadSheet />)} />
							<Route path="TUIGrid" element={getFallBackUI(<TUIGrid />)} />
						</Routes>
					</BrowserRouter >
				</Provider>
				{/* <img src="https://lh3.googleusercontent.com/viEyb7P5pHqDK_oxe__VvU8BVZiaItf7KrWz3BCdV2edXC3ujTtiFQPrHxWZ1YZ6tPBotIuQfNVC6xaVdfLr=w1920-h912-rw"/> */}

			</div>
		);
	}
}

export default App;

// function App() {
//   return (
//     <div classNameName="App">
//       <HelloWorld />
//       <MyPivotTable/>
//     </div>
//   );
// }

// export default App;

//  ============================================
// import React from "react";

// const App: React.FC = () => <div>Hello world!</div>;

// export default App;