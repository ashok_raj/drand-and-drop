// @ts-nocheck


import React, { useEffect } from "react";
import tui from 'tui-grid';


console.log('tui', tui)
const TUIGrid = () => {

    useEffect(() => {
        renderTable();
    }, []);


    const renderTable = () => {
        const grid = new tui({
            el: document.getElementById('grid'),
            data: [
                {
                    name: 'Beautiful Lies',
                    artist: 'Birdy',
                    release: '2016.03.26',
                    genre: 'Pop',

                    _children: [{
                        name: '_child Beautiful Lies',
                        artist: '_child Birdy',
                        release: '_child 2016.03.26',
                        genre: '_child Pop',
                    }

                    ]
                },
                {
                    name: 'Beautiful Lies',
                    artist: 'Birdy',
                    release: '2016.03.26',
                    genre: 'Pop'
                }, {
                    name: 'Beautiful Lies',
                    artist: 'Birdy',
                    release: '2016.03.26',
                    genre: 'Pop',
                    _children: [{
                        name: '_child Beautiful Lies',
                        artist: '_child Birdy',
                        release: '_child 2016.03.26',
                        genre: '_child Pop',
                        _children: [{
                            name: '_child Beautiful Lies',
                            artist: '_child Birdy',
                            release: '_child 2016.03.26',
                            genre: '_child Pop',
                        }

                        ]
                    }]
                }, {
                    name: 'Beautiful Lies',
                    artist: 'Birdy',
                    release: '2016.03.26',
                    genre: 'Pop'
                }, {
                    name: 'Beautiful Lies',
                    artist: 'Birdy',
                    release: '2016.03.26',
                    genre: 'Pop'
                }
            ],
            // scrollX: false,
            // scrollY: false,
            rowHeaders: ['checkbox'],
            bodyHeight: 500,
      
            treeColumnOptions: {
                name: 'artist',
                useCascadingCheckbox: true,
                useIcon:true,
                indentWidth:10
            },
            columns: [
                {
                    header: 'Name',
                    name: 'name'
                },
                {
                    header: 'Artist',
                    name: 'artist'
                },
                // {
                //     header: 'Type',
                //     name: 'type'
                // },
                {
                    header: 'Release',
                    name: 'release'
                },
                {
                    header: 'Genre',
                    name: 'genre'
                }
            ]
        });

        grid.on('expand', ev => {
            const { rowKey } = ev;
            const descendantRows = grid.getDescendantRows(rowKey);

            console.log('rowKey: ' + rowKey);
            console.log('descendantRows: ' + descendantRows);

            if (!descendantRows.length) {
                grid.appendRow(
                    {
                        name: 'dynamic loading data',
                        _children: [
                            {
                                name: 'leaf row'
                            },
                            {
                                name: 'internal row',
                                _children: []
                            }
                        ]
                    },
                    { parentRowKey: rowKey }
                );
            }
        });

        grid.on('collapse', ev => {
            const { rowKey } = ev;
            const descendantRows = grid.getDescendantRows(rowKey);

            console.log('rowKey: ' + rowKey);
            console.log('descendantRows: ' + descendantRows);
        });


    }

    return (
        <div>
            <h1>TUIGrid</h1>
            <div id="grid"></div>

        </div>

    )
}
export default TUIGrid;
