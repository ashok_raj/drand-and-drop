// @ts-nocheck


import React, { useEffect, useState } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { jsonData } from "./assets/jsonData";
import { ResultJSON } from "./components/ResultJSON";
import MyTable, { calculatedMeasuers } from "./MyTable";

import queryGeneration from "./assets/queryGeneration";
import uuid from "uuid/v4";

let setSelectedFieldChipsRef;
let selectedFieldRef;

const itemsFromBackend = jsonData.fields.map((data) => {
    return { id: uuid(), content: data };
});

console.log("itemsFromBackend ", itemsFromBackend);

const measuresIndex = itemsFromBackend.push({ id: uuid(), content: "Measures" });


let formattedArr: any = [];
const rowData = jsonData.data;
const header = jsonData.fields;
rowData.forEach((row) => {
    let obj = {};
    row.forEach((element, index) => {
        obj[header[index]] = element;
    })
    formattedArr.push(obj)
})



console.log("formattedArr ", formattedArr)



let dataObj = {};

itemsFromBackend.forEach((it, i) => {
    dataObj[i] = [];
})

jsonData.data.map((dataArr) => {
    dataArr.forEach((d, i) => {
        if (!dataObj[i].includes(d))
            dataObj[i].push(d);
    })
});


calculatedMeasuers.forEach((calcObj) => {
    dataObj[measuresIndex - 1].push(calcObj.as)
})


console.log("dataObj ", dataObj);



const columnsFromBackend = {
    ["fields"]: {
        name: "Fields",
        items: itemsFromBackend
    },
    ["row"]: {
        name: "Row",
        items: []
    },
    ["column"]: {
        name: "Column",
        items: []
    },
    ["values"]: {
        name: "Values",
        items: []
    }, ["page"]: {
        name: "Page",
        items: []
    }
};

const onDragEnd = (result, columns, setColumns) => {

    // console.log("result ", result);
    // console.log("columns ", columns);

    if (!result.destination) return;
    const { source, destination } = result;

    if (source.droppableId !== destination.droppableId) {
        const sourceColumn = columns[source.droppableId];
        const destColumn = columns[destination.droppableId];
        const sourceItems = [...sourceColumn.items];
        const destItems = [...destColumn.items];
        const [removed] = sourceItems.splice(source.index, 1);

        console.log("Removed ", removed)

        if (removed.content === "Measures" && (destination.droppableId !== 'values' && destination.droppableId !== 'fields')) {
            return;
        }


        destItems.splice(destination.index, 0, removed);
        setColumns({
            ...columns,
            [source.droppableId]: {
                ...sourceColumn,
                items: sourceItems
            },
            [destination.droppableId]: {
                ...destColumn,
                items: destItems
            }
        });
    } else {
        const column = columns[source.droppableId];
        const copiedItems = [...column.items];
        const [removed] = copiedItems.splice(source.index, 1);
        copiedItems.splice(destination.index, 0, removed);
        setColumns({
            ...columns,
            [source.droppableId]: {
                ...column,
                items: copiedItems
            }
        });
    }
};


const removeItem = (itemId, columnId, columns, setColumns) => {
    // console.log("Remove Item", itemId, columnId);

    const columnCopy = { ...columns };
    // console.log(columnCopy);

    // console.log(columnCopy[columnId].items);
    let itemIndex;
    const removedItem = columnCopy[columnId].items.find((item, index) => {
        if (item.id === itemId) {
            itemIndex = index;
            return true;
        }
    })
    columnCopy[columnId].items.splice(itemIndex, 1);
    columnCopy.fields.items.push(removedItem)
    setColumns(columnCopy)

}


const getColumnFields = (fieldName, area, columns) => {
    // console.log('i ', fieldName, area, itemsFromBackend);

    // if (area === 'Fields' || area === 'Values') {

    // console.log("columns ", columns)
    if (area === 'Fields') {
        return;
    }


    if (fieldName === "Measures") {
        const { row, column } = columns;
        if (row.items.length === 0 && column.items.length === 0)
            return;
    }

    let fieldIndex;

    itemsFromBackend.find((item, i) => {
        // console.log(item)
        if (item.content === fieldName) {
            fieldIndex = i;
            return true;
        }
    })
    // itemsFromBackend



    selectedFieldRef(itemsFromBackend[fieldIndex].content);
    setSelectedFieldChipsRef(dataObj[fieldIndex])

}

const DroppableDiv = ({ column, columnId, customStyle = {}, columns, setColumns, resultJSON }) => {


    return <div key={columnId} className="droppable-container">
        <Droppable droppableId={columnId} key={columnId}>
            {(provided, snapshot) => {
                return (
                    <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        style={{
                            ...{
                                background: snapshot.isDraggingOver
                                    ? "lightblue"
                                    : "lightgrey",
                                padding: 4,
                                width: 150,
                                // minHeight: 500
                            }, ...customStyle
                        }}
                    >
                        {column.items.map((item, index) => {
                            return (
                                <Draggable
                                    key={item.id}
                                    draggableId={item.id}
                                    index={index}
                                >
                                    {(provided, snapshot) => {
                                        return (
                                            <div
                                                onClick={(e) => {
                                                    if (e?.target?.id === 'remove')
                                                        return;
                                                    getColumnFields(item.content, column.name, columns)
                                                }}
                                                ref={provided.innerRef}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                                style={{
                                                    userSelect: "none",
                                                    padding: 2,
                                                    margin: "4px 4px 4px 4px",
                                                    minHeight: "18px",
                                                    backgroundColor: snapshot.isDragging
                                                        ? "#263B4A"
                                                        : "#456C86",
                                                    color: "white",
                                                    ...provided.draggableProps.style
                                                }}
                                            >
                                                <span> {item.content}</span>
                                                {(column.name === 'Row' || column.name === 'Column' || column.name === 'Page') &&
                                                    <span className="ml-3" id="remove" onClick={() => {
                                                        removeItem(item.id, columnId, columns, setColumns)
                                                    }}>X</span>}
                                            </div>
                                        );
                                    }}
                                </Draggable>
                            );
                        })}
                        {provided.placeholder}
                    </div>
                );
            }}
        </Droppable>
    </div>

}


function Main() {
    const [columns, setColumns] = useState(columnsFromBackend);
    const [selectedField, setSelectedField] = useState('');
    const [selectedFieldChips, setSelectedFieldChips] = useState([]);

    setSelectedFieldChipsRef = setSelectedFieldChips;
    selectedFieldRef = setSelectedField;
    const fieldsArr = Object.entries(columns);
    const columnId1 = fieldsArr[0][0];
    const column1 = fieldsArr[0][1];
    const columnId2 = fieldsArr[1][0];
    const column2 = fieldsArr[1][1];
    const columnId3 = fieldsArr[2][0];
    const column3 = fieldsArr[2][1];
    const columnId4 = fieldsArr[3][0];
    const column4 = fieldsArr[3][1];
    const columnId5 = fieldsArr[4][0];
    const column5 = fieldsArr[4][1];


    const [resultJSON, setResultJSON] = useState({
        "reportParameters": [],
        "isGDF": true,
        "isHDF": true,
        "calculatedMeasures": []
    });


    useEffect(() => {
        console.log("formattedArr",formattedArr);
        console.log("resultJSON",resultJSON);
        queryGeneration(formattedArr, resultJSON)
    }, [resultJSON]);

    return (

        <div >
            <div className="mb-2 dragdrop-container">
                <DragDropContext onDragEnd={result => onDragEnd(result, columns, setColumns)} >
                    <div className="dd-container">
                        <div className="dd-fields-container">
                            <div className="fields-text">Fields</div>
                            <DroppableDiv column={column1} columnId={columnId1} columns={columns} setColumns={setColumns}
                                customStyle={{ overflow: 'auto' }}
                                resultJSON={resultJSON} />

                            <div className="fields-text">Filters</div>
                            <MyTable selectedField={selectedField} selectedFieldChips={selectedFieldChips} columns={columns} setResultJSON={setResultJSON} />
                        </div>
                        <div className="dd-axes-container">
                            <div className="axis-container">
                                <div className="axis-label">Page</div>
                                <DroppableDiv column={column5} columnId={columnId5} columns={columns} setColumns={setColumns}
                                    resultJSON={resultJSON}
                                    customStyle={{ minHeight: 40, flexGrow: 0, flexShrink: 1, display: 'flex', width: '100%' }} />
                            </div>
                            <div className="axis-container">
                                <div className="axis-label">Row</div>
                                <DroppableDiv column={column2} columnId={columnId2}
                                    customStyle={{ minHeight: 40, flexGrow: 0, flexShrink: 1, display: 'flex', width: '100%' }} columns={columns} setColumns={setColumns} resultJSON={resultJSON} />
                            </div>
                            <div className="axis-container">
                                <div className="axis-label">Columns</div>
                                <DroppableDiv column={column3} columnId={columnId3}
                                    customStyle={{ minHeight: 40, flexGrow: 0, flexShrink: 1, display: 'flex', width: '100%' }} columns={columns} setColumns={setColumns} resultJSON={resultJSON} />
                            </div>
                            <div className="axis-container">
                                <div className="axis-label">Values</div>
                                <DroppableDiv column={column4} columnId={columnId4}
                                    customStyle={{ minHeight: 40, flexGrow: 0, flexShrink: 1, display: 'flex', width: '100%' }} columns={columns} setColumns={setColumns} resultJSON={resultJSON} />
                            </div>
                            <div className="mt-3 sf" id="DynamicReport"></div>
                        </div>
                    </div>
                </DragDropContext>
            </div>
        </div>
    );
}

export default Main;


