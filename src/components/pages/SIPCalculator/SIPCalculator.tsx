import React, { useEffect } from "react";



const SIPCalculator = () => {

    useEffect(() => {
        calculateReturns();
    }, []);

    const calculateReturns = () => {
        const years = 10;
        const investment = 10000;
        const profitPercent = 12;



        let total = investment;

        for (var i = 0; i < years; i++) {
            const currentMonthProfit = ((profitPercent /100 ) *total );
            console.log("currentMonthProfit ", currentMonthProfit);
            total = total + currentMonthProfit;
        }

        console.log("total ", total);



    }

    return (<h1>SIP Calculator</h1>)
}

export default SIPCalculator;