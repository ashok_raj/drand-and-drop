// @ts-nocheck
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";

import React, { useEffect, useState } from "react";
import './CollapseMenu.css'

import FilterSection, { calculatedMeasuers } from "../FilterSection/FilterSection";

import { getItemsFromBackend, getDataObj, getFormattedArr } from "../../helpers/Analysis/AnalysisHelper";
import queryGeneration from "../../helpers/Analysis/QueryGenerationHelper";
import uuid from "uuid/v4";

// import CloseIcon from './CloseIcon.jpg';
// import CloseIcon from './CloseIcon.jpg';


// import CloseIcon from '@material-ui/icons/Close';

let setSelectedFieldChipsRef;
let selectedFieldRef;

const MenuList = () => {
    return (
        <div className="mt-3 mr-3">
            <h4 className="mt-1" >
                Menu List Item
            </h4>
        </div>
    );
};

const IconArrowUp = () => {
    return (
        <span className="ml-1">
            ^
            {/* <KeyboardArrowUpIcon/> */}
        </span>
    );
};

const IconArrowRight = () => {
    return (
        <span className="float-right">
            {'>'}
            {/* <KeyboardArrowRightIcon/> */}
        </span>
    );
};

const removeItem = (itemId, columnId, columns, setColumns, source) => {
    // const columnCopy = { ...columns };
    const columnCopy = deepCopy(columns);

    // console.log(columnCopy[columnId].items);
    let itemIndex;
    const removedItem = columnCopy[columnId].items.find((item, index) => {
        if (item.id === itemId) {
            itemIndex = index;
            return true;
        }
    })
    columnCopy[columnId].items.splice(itemIndex, 1);
    // columnCopy.fields.items.push(removedItem)
    // columnCopy[source].items.push(removedItem)
    setColumns(columnCopy)
}


const getColumnFields = (fieldName, area, columns, source) => {


    if (source === 'measures') {
        return;
    }


    const itemsFromBackend = getItemsFromBackend();
    const dataObj = getDataObj();

    // console.log('i ', fieldName, area, itemsFromBackend);

    // if (area === 'Fields' || area === 'Values') {

    // console.log("columns ", columns)
    if (area === 'Fields') {
        return;
    }


    if (fieldName === "Measures") {
        const { row, column } = columns;
        if (row.items.length === 0 && column.items.length === 0)
            return;
    }

    let fieldIndex;

    itemsFromBackend.find((item, i) => {
        // console.log(item)
        if (item.content === fieldName) {
            fieldIndex = i;
            return true;
        }
    })
    // itemsFromBackend



    selectedFieldRef(itemsFromBackend[fieldIndex].content);
    setSelectedFieldChipsRef(dataObj[fieldIndex])

}


const removeItemWithCheckbox = (item, columns, setColumns) => {
    // console.log("removeItemWithCheckbox ", item)
    // console.log("columns ", columns);

    const columnCopy = deepCopy(columns);
    let chipFound = false;

    for (const key of ['column', 'row', 'values']) {

        if (chipFound)
            break;

        for (const [i, chip] of columns[key].items.entries()) {
            console.log(key, chip);
            if (chip.content === item.content) {
                columnCopy[key].items.splice(i, 1);
                chipFound = true;
                break;
            }
        }
    }

    setColumns(columnCopy);
}

const DroppableDiv = ({ column, columnId, customStyle = {}, columns, setColumns, chipStyle = {}, chipTextStyle = {}, itemsFromBackend }) => {


    let chipArr = [];


    if (column.name === 'Fields' || column.name === 'measures') {
        ['column', 'row', 'values'].forEach((fieldKey) => {
            columns[fieldKey].items.forEach((chipObj) => {
                chipArr.push(chipObj)
            })
        })
    }

    console.log("chipArr ", chipArr)

    return <div key={columnId} className="droppable-container">
        <Droppable droppableId={columnId} key={columnId}>
            {(provided, snapshot) => {
                return (
                    <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        style={{
                            ...{
                                // background: snapshot.isDraggingOver
                                //     ? "lightblue"
                                //     // : "lightgrey",
                                //     : "#f6f6f6",

                                padding: 4,
                                width: column.name === 'Fields' ? 230 : 150,
                                // minHeight: 500
                            }, ...customStyle
                        }}
                    >
                        {column.items.map((item, index) => {

                            let checked = false;
                            if (column.name === 'Fields' || column.name === 'measures') {
                                const found = chipArr.find((chipObj) => chipObj.content === item.content);
                                if (found) {
                                    checked = true;
                                }
                            }

                            return (
                                <Draggable
                                    key={item.id}
                                    draggableId={item.id}
                                    index={index}
                                >
                                    {(provided, snapshot) => {
                                        return (
                                            <div
                                                onClick={(e) => {
                                                    if (e?.target?.id === 'remove')
                                                        return;
                                                    getColumnFields(item.content, column.name, columns, item.source)
                                                }}
                                                ref={provided.innerRef}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                                style={{
                                                    userSelect: "none",
                                                    margin: "1px",
                                                    fontSize: '7px',
                                                    width: 'auto',
                                                    maxWidth: 'fit-content',
                                                    // backgroundColor: snapshot.isDragging
                                                    //     ? "#7cbdeb"
                                                    //     // : "#456C86",
                                                    //     : "white",
                                                    // // color: "white",
                                                    // color: "black",
                                                    ...chipStyle,

                                                    ...provided.draggableProps.style
                                                }}
                                            >
                                                {(column.name === 'Fields' || column.name === 'measures') && <span><input className="fieldsCheckBox" onChange={(e) => { if (checked) removeItemWithCheckbox(item, columns, setColumns) }} type="checkbox" checked={checked} /></span>}
                                                <span style={chipTextStyle} className="contentStyle"> {item.content}</span>
                                                {/* {column.name === 'Fields' && <span>abc</span>} */}
                                                {(column.name === 'Row' || column.name === 'Column' || column.name === 'Values') &&
                                                    <span className={`ml-1 ${item.id}`} id="remove" onClick={() => {
                                                        removeItem(item.id, columnId, columns, setColumns, item.source)
                                                    }}><span style={{ display: 'none' }}>|</span>
                                                        {/* <span className="chip-remove-icon">&#x2715;</span> */}
                                                        <span style={{ paddingLeft: '4px' }}>
                                                            {/* <img style={{height: '10px'}} src={CloseIcon}/> */}
                                                            X
                                                        </span>
                                                    </span>
                                                }

                                            </div>
                                        );
                                    }}
                                </Draggable>
                            );
                        })}
                        {provided.placeholder}
                    </div>
                );
            }}
        </Droppable>
    </div>

}


const deepCopy = (obj) => {
    return JSON.parse(JSON.stringify(obj));
}

const onDragEnd = (result, columns, setColumns) => {



    // console.log("result ", result);
    // console.log("result ", columns);


    if (!result.destination) return;


    const { source, destination } = result;



    console.log('destination.droppableId ',destination.droppableId)
    if (source.droppableId !== destination.droppableId) {

        const start = source.droppableId;
        const end = destination.droppableId;

        if ((start === "fields" && end === "measures") || (start === "fields" && end === "measures")) {
            return;
        }

        // if (destination.droppableId === "fields") return;

        const sourceColumn = columns[source.droppableId];
        const destColumn = columns[destination.droppableId];
        const sourceItems = [...sourceColumn.items];
        const destItems = [...destColumn.items];

        let removed;

        if (source.droppableId === 'fields' || source.droppableId === 'measures') {
            removed = deepCopy(sourceItems[source.index]);
            removed.id = uuid();
        } else {
            removed = sourceItems.splice(source.index, 1)[0];
        }

        console.log("Removed ", removed);

        if(end === "fields" || end === "measures") {
            setTimeout(()=>{
                document.getElementsByClassName(`${removed.id}`)[0].click();
            })
            return;
        }
        // const [removed] = sourceItems.splice(source.index, 1);
        // const removed = deepCopy(sourceItems[source.index]);
        // console.log("Removed ", removed);
        // removed.id = uuid();

        if (removed.content === "Measures" && (destination.droppableId !== 'values' && destination.droppableId !== 'fields')) {
            return;
        }


        destItems.splice(destination.index, 0, removed);
        setColumns({
            ...columns,
            [source.droppableId]: {
                ...sourceColumn,
                items: sourceItems
            },
            [destination.droppableId]: {
                ...destColumn,
                items: destItems
            }
        });




    } else {
        const column = columns[source.droppableId];
        const copiedItems = [...column.items];
        const [removed] = copiedItems.splice(source.index, 1);
        copiedItems.splice(destination.index, 0, removed);
        setColumns({
            ...columns,
            [source.droppableId]: {
                ...column,
                items: copiedItems
            }
        });
    }
};




const CollapseMenu = ({ setAccordian, columnsFromBackend, itemsFromBackend }) => {


    const [selectedField, setSelectedField] = useState('');
    const [selectedFieldChips, setSelectedFieldChips] = useState([]);

    setSelectedFieldChipsRef = setSelectedFieldChips;
    selectedFieldRef = setSelectedField;

    const [resultJSON, setResultJSON] = useState({
        "reportParameters": [],
        "isGDF": true,
        "isHDF": true,
        "calculatedMeasures": []
    });

    const [menuArray, toggleMenuArray] = useState([1, 2]);
    const [columns, setColumns] = useState(columnsFromBackend);
    const fieldsArr = Object.entries(columns);



    const fieldsId = fieldsArr[0][0];
    const fieldBox = fieldsArr[0][1];
    const rowId = fieldsArr[1][0];
    const rowBox = fieldsArr[1][1];
    const columnID = fieldsArr[2][0];
    const columnBox = fieldsArr[2][1];
    const valuesId = fieldsArr[3][0];
    const valuesBox = fieldsArr[3][1];
    const measuresId = fieldsArr[4][0];
    const measuresBox = fieldsArr[4][1];

    console.log({ measuresBox })


    const transformMenu = (menu, action) => {
        return false;
        let copy = menuArray.slice();
        if (action === 'open' && !copy.includes(menu)) {
            copy.push(menu);
        }

        if (action === 'close') {
            const index = copy.indexOf(menu);
            copy.splice(index, 1);
        }
        toggleMenuArray(copy);

    }

    useEffect(() => {
        if (menuArray.length === 3) {
            transformMenu(menuArray[0], 'close');
        }
        setAccordian(menuArray.length);
    }, [menuArray])


    useEffect(() => {
        console.log("formattedArr", getFormattedArr());
        console.log("resultJSON", resultJSON)

        queryGeneration(getFormattedArr(), resultJSON)
    }, [resultJSON]);

    useEffect(() => {
        // dispatchEvent(save)
    }, [columns]);



    const analysisBoxStyle = { overflow: 'auto', marginBottom: '14px', minHeight: '7rem', border: '1px dashed #a4a1a1', borderRadius: '10px', backgroundColor: '#f7f6f6' };
    const analysisChipStyle = { borderRadius: '5px', border: '1px groove #a4a1a1', backgroundColor: 'white', paddingLeft: '6px', paddingRight: '6px', minHeight: "18px", padding: '2px' };
    const analysisChipTextStyle = { fontSize: '9px', paddingLeft: '1px', borderRight: '1px solid black', paddingRight: '8px' }

    const fieldsChipStyle = { minHeight: "17px" };
    const fieldsChipTextStyle = { fontSize: '9px', fontWeight: '500' }


    const fieldsBoxStyle = { overflowY: 'auto', maxHeight: '20rem' };


    let accordianGapMargin;
    if (menuArray.length === 0) {
        accordianGapMargin = '5rem';
    }

    else if (menuArray.length === 1) {
        if (menuArray.includes(1)) {
            accordianGapMargin = '4rem';
        } else {
            accordianGapMargin = '2rem';
        }
    }

    let accordianGap = { marginLeft: accordianGapMargin };

    return (

        <div>
            <DragDropContext onDragEnd={result => onDragEnd(result, columns, setColumns)} >


                <div className={`menuMain boxBorder`} style={accordianGap}>
                    {/* <div className={`pt-5 ${menuArray.includes(0) ? 'pr-4' : ''}`}>
                        {!menuArray.includes(0) && <div className="menuItem handIcon pt-5 columnHeading" onClick={() => { transformMenu(0, 'open') }}><span>Drivers</span> <IconArrowUp /></div>}
                        {
                            menuArray.includes(0) &&
                            <div>
                                <div className="handIcon columnHeading" onClick={() => { transformMenu(0, 'close') }}>Drivers <IconArrowRight /></div>
                                <MenuList />
 
                            </div>
 
                        }
                    </div> */}
                    {/* className="pl-4 pt-5 pr-3 borderLeft" */}
                    <div className={`pt-5 borderLeft ${menuArray.includes(1) ? 'pl-4 pr-4' : ''}`}>
                        {!menuArray.includes(1) && <div className="menuItem handIcon columnHeading" onClick={() => { transformMenu(1, 'open') }}><span>Analysis</span> <IconArrowUp /></div>}
                        {
                            menuArray.includes(1) &&
                            <div>
                                <div className="handIcon columnHeading" onClick={() => { transformMenu(1, 'close') }}><span>Fields</span></div>
                                {/* <MenuList /> */}
                                <div className="filtersBox mt-3">
                                    <div className="boxHeading">Filters </div>
                                    {/* <DroppableDiv column={filtersBox} columnId={filtersId} columns={columns} setColumns={setColumns}
                                        customStyle={analysisBoxStyle} chipStyle={analysisChipStyle}
                                    />  */}
                                    <div className="filterDiv">
                                        <FilterSection selectedField={selectedField} selectedFieldChips={selectedFieldChips} columns={columns} setResultJSON={setResultJSON} />

                                    </div>
                                </div>
                                <div className="columnBox mt-3">
                                    <div className="boxHeading">Columns </div>
                                    <DroppableDiv column={columnBox} columnId={columnID} columns={columns} setColumns={setColumns}
                                        customStyle={analysisBoxStyle} chipStyle={analysisChipStyle} itemsFromBackend={itemsFromBackend}
                                        chipTextStyle={analysisChipTextStyle}
                                    />
                                </div>
                                <div className="valuesBox">
                                    <div className="boxHeading">Values</div>
                                    <DroppableDiv column={valuesBox} columnId={valuesId} columns={columns} setColumns={setColumns}
                                        customStyle={analysisBoxStyle} chipStyle={analysisChipStyle} itemsFromBackend={itemsFromBackend}
                                        chipTextStyle={analysisChipTextStyle}
                                    />
                                </div>
                                <div className="rowBox">
                                    <div className="boxHeading">Row</div>
                                    <DroppableDiv column={rowBox} columnId={rowId} columns={columns} setColumns={setColumns}
                                        customStyle={analysisBoxStyle} chipStyle={analysisChipStyle} itemsFromBackend={itemsFromBackend}
                                        chipTextStyle={analysisChipTextStyle}
                                    />
                                </div>
                            </div>

                        }
                    </div>
                    <div className={`pt-5 borderLeft ${menuArray.includes(2) ? 'pl-4' : ''}`}>
                        {!menuArray.includes(2) && <div className="menuItem handIcon columnHeading" onClick={() => { transformMenu(2, 'open') }}><span>Fields</span> <IconArrowUp /></div>}
                        {
                            menuArray.includes(2) &&
                            <div>
                                {/* <div className="handIcon mb-3 columnHeading" onClick={() => { transformMenu(2, 'close') }}>Fields <IconArrowRight /></div> */}


                                <div className="mb-3">
                                    <input type="text" className="form-control" placeholder="&#xf52a; Search " />
                                </div>

                                <div className="mb-3 boxHeading">
                                    <span><img src="https://i.im.ge/2022/07/06/u91jp0.png"
                                        style={{
                                            height: '12px',
                                            marginBottom: '3px',
                                            marginRight: '3px'
                                        }}
                                    /></span>
                                    <span> DATA - Payroll ({columns?.fields?.items?.length})</span>
                                    {/* <span><a href="javascript:void(0)">Configure</a></span> */}
                                </div>

                                <DroppableDiv column={fieldBox} columnId={fieldsId} columns={columns} setColumns={setColumns}
                                    customStyle={fieldsBoxStyle}
                                    chipStyle={fieldsChipStyle}
                                    chipTextStyle={fieldsChipTextStyle}
                                />

                                {/* <div className="mb-3 boxHeading">Measures ({columns?.measures?.items?.length}) </div> */}

                                <div className="mt-3">
                                    <span><img style={{ height: '13px' }} src="https://uxwing.com/wp-content/themes/uxwing/download/18-education-school/formula-fx.png" /></span>
                                    <span className="ml-2" style={{ fontWeight: 'bold' }}>Measures({columns?.measures?.items?.length})</span>
                                    <span style={{
                                        marginLeft: "12px",
                                        marginLeft: '63px',
                                        transform: 'rotate(90deg)',
                                        fontSize: '17px',
                                        fontWeight: 'bold',
                                        display: 'inline-block'
                                    }}>
                                        {'>'}
                                    </span>
                                </div>



                                <DroppableDiv column={measuresBox} columnId={measuresId} columns={columns} setColumns={setColumns}
                                    customStyle={fieldsBoxStyle}
                                    chipStyle={fieldsChipStyle}
                                    chipTextStyle={fieldsChipTextStyle}
                                />



                            </div>

                        }
                    </div>
                </div>
            </DragDropContext>
        </div>
    )
}

export default React.memo(CollapseMenu);





