// @ts-nocheck

import React, { useEffect, useState } from "react";
import './FilterSection.css';

export const calculatedMeasuers = [{ "pkMeasureId": 343, "measureName": "SRCost", "measureDescription": "", "measure": "{\r\n  \"as\" : \"SRCost\",\r\n  \"func\" : \"sum\",\r\n  \"func_params\" : [ ],\r\n  \"expr\" : \"[Total Actual CTC]\"\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 344, "measureName": "SalesAchieved", "measureDescription": "", "measure": "{\r\n  \"as\" : \"SalesAchieved\",\r\n  \"func\" : \"sum\",\r\n  \"func_params\" : [ ],\r\n  \"expr\" : \"[Actual_Sales]\"\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 345, "measureName": "SalesTarget", "measureDescription": "", "measure": "{\r\n  \"as\" : \"SalesTarget\",\r\n  \"func\" : \"sum\",\r\n  \"func_params\" : [ ],\r\n  \"expr\" : \"[Sales_Target]\"\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 346, "measureName": "LostCount", "measureDescription": "", "measure": "{\r\n  \"as\" : \"LostCount\",\r\n  \"func\" : \"sum\",\r\n  \"func_params\" : [ ],\r\n  \"expr\" : \"[Lost Count]\"\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 347, "measureName": "WonCount", "measureDescription": "", "measure": "{\r\n  \"as\" : \"WonCount\",\r\n  \"func\" : \"sum\",\r\n  \"func_params\" : [ ],\r\n  \"expr\" : \"[Won Count]\"\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 348, "measureName": "DC", "measureDescription": "", "measure": "{\r\n  \"as\" : \"DC\",\r\n  \"func\" : \"sum\",\r\n  \"func_params\" : [ ],\r\n  \"expr\" : \"[Days to Close]\"\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 349, "measureName": "DealCount", "measureDescription": "", "measure": "{\r\n  \"as\" : \"DealCount\",\r\n  \"func\" : \"sum\",\r\n  \"func_params\" : [ ],\r\n  \"expr\" : \"([Won Count]+[Lost Count])\"\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 350, "measureName": "DealConversionRate", "measureDescription": "", "measure": "{\r\n  \"as\" : \"DealConversionRate\",\r\n  \"func\" : \"\",\r\n  \"func_params\" : [ ],\r\n  \"expr\" : \"([WonCount]/[DealCount])*100\"\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 351, "measureName": "AvgOrdVal-WonOnly", "measureDescription": "", "measure": "{\r\n  \"as\" : \"AvgOrdVal-WonOnly\",\r\n  \"func\" : \"\",\r\n  \"func_params\" : [ ],\r\n  \"format\" : {\r\n    \"type\" : \"NumberFormat\",\r\n    \"locale\" : \"en-US\",\r\n    \"options\" : {\r\n      \"maximumFractionDigits\" : 3\r\n    }\r\n  },\r\n  \"expr\" : \"([SalesAchieved]/[WonCount])\"\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 352, "measureName": "AverageTimeToClose(days)", "measureDescription": "", "measure": "{\r\n  \"as\" : \"AverageTimeToClose(days)\",\r\n  \"func\" : \"\",\r\n  \"func_params\" : [ ],\r\n  \"expr\" : \"([DC]/[DealCount])\"\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 353, "measureName": "SRCost vs SalesAchieved", "measureDescription": "", "measure": "{\r\n  \"as\" : \"SRCost vs SalesAchieved\",\r\n  \"func\" : \"\",\r\n  \"func_params\" : [ ],\r\n  \"format\" : {\r\n    \"type\" : \"NumberFormat\",\r\n    \"locale\" : \"en-US\",\r\n    \"options\" : {\r\n      \"style\" : \"percent\",\r\n      \"minimumFractionDigits\" : 1\r\n    }\r\n  },\r\n  \"expr\" : \"([SRCost]/[SalesAchieved])*100\"\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 354, "measureName": "Sales Achivement", "measureDescription": "", "measure": "{\r\n  \"as\" : \"Sales Achivement\",\r\n  \"func\" : \"\",\r\n  \"func_params\" : [ ],\r\n  \"expr\" : \"([SalesAchieved]/[SalesTarget])*100\"\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 355, "measureName": "RankTimeToCloseDays", "measureDescription": "", "measure": "{\r\n  \"as\" : \"RankTimeToCloseDays\",\r\n  \"func\" : \"ranko\",\r\n  \"func_params\" : [ {\r\n    \"func_params\" : 0\r\n  }, {\r\n    \"func\" : \"orderBy\",\r\n    \"isSql\" : true,\r\n    \"func_params\" : [ \"Department\", [ \"AverageTimeToClose(days)\" ] ]\r\n  } ]\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 356, "measureName": "CalcScore", "measureDescription": "", "measure": "{\r\n  \"as\" : \"CalcScore\",\r\n  \"func\" : \"\",\r\n  \"func_params\" : [ ],\r\n  \"expr\" : \"([SRCost vs SalesAchieved]*0.15)+([Sales Achivement]*0.5)+([AvgOrdVal-WonOnly]*0.1)+([DealConversionRate]*0.1)+([AverageTimeToClose(days)]*0.15)\"\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 357, "measureName": "Over All Rank", "measureDescription": "", "measure": "{\r\n  \"as\" : \"Over All Rank\",\r\n  \"func\" : \"ranko\",\r\n  \"func_params\" : [ {\r\n    \"func_params\" : 0\r\n  }, {\r\n    \"func\" : \"orderBy\",\r\n    \"isSql\" : true,\r\n    \"func_params\" : [ [ \"CalcScore\", \"DESC\" ] ]\r\n  } ]\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 358, "measureName": "Cal_Rank_E", "measureDescription": "", "measure": "{\r\n  \"as\" : \"Cal_Rank_E\",\r\n  \"func\" : \"ranko\",\r\n  \"func_params\" : [ {\r\n    \"func_params\" : 0\r\n  }, {\r\n    \"func\" : \"orderBy\",\r\n    \"isSql\" : true,\r\n    \"func_params\" : [ [ \"SRCost vs SalesAchieved\", \"DESC\" ] ]\r\n  } ]\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 359, "measureName": "Rank within Region", "measureDescription": "", "measure": "{\r\n  \"as\" : \"Rank within Region\",\r\n  \"func\" : \"ranko\",\r\n  \"func_params\" : [ {\r\n    \"func\" : \"partitionBy\",\r\n    \"isSql\" : true,\r\n    \"func_params\" : [ \"Department\" ]\r\n  }, {\r\n    \"func\" : \"orderBy\",\r\n    \"isSql\" : true,\r\n    \"func_params\" : [ [ \"CalcScore\", \"DESC\" ] ]\r\n  } ]\r\n}", "parentId": 0, "modelId": 102 }, { "pkMeasureId": 360, "measureName": "WonCount Percent Rank", "measureDescription": "", "measure": "{\r\n  \"as\" : \"WonCount Percent Rank\",\r\n  \"func\" : \"percentRank\",\r\n  \"func_params\" : [ {\r\n    \"func_params\" : 65\r\n  }, {\r\n    \"func\" : \"orderBy\",\r\n    \"isSql\" : true,\r\n    \"func_params\" : [ [ \"Won Count\", \"DESC\" ] ]\r\n  } ]\r\n}", "parentId": 0, "modelId": 102 }];

const formatApiDataToCalcMeasures = (measuresByModel) => {
    if (!measuresByModel) {
        measuresByModel = [];
    }
    let calcMeasures = [];
    for (let i = 0; i < measuresByModel.length; i++) {
        calcMeasures.push(JSON.parse(measuresByModel[i].measure));
    }

    return calcMeasures;
}



const FilterSection = ({ selectedField, selectedFieldChips, columns, setResultJSON }) => {


    // console.log("columns ", columns);

    const [markedFields, setMarkedFields] = useState({});


    const markField = (selected, fieldName) => {

        // console.log("markedFields ", markedFields)
        // console.log('selected',selected);
        // console.log('selectedField', selectedField);
        // console.log('fieldName',fieldName);

        const copy = { ...markedFields };

        if (selected) {
            if (!copy[selectedField]) {
                copy[selectedField] = [];
            }

            copy[selectedField].push(fieldName);

        } else {
            const fieldIndex = copy[selectedField].indexOf(fieldName);
            copy[selectedField].splice(fieldIndex, 1);
        }
        setMarkedFields(copy);
    }


    useEffect(() => {
        let jsonArray: any = [];

        let measureChips: any = [];

        ['row', 'column', 'values', 'page'].forEach((columnName) => {

            columns[columnName]?.items.forEach(({ content }) => {

                // if (columnName === 'values' && content == "Measures") {
                if (columnName === 'values') {
                    // jsonArray.push({
                    //     "axis": "Column",
                    //     "dimensionName": "Measures",
                    //     "dimensionValues": markedFields[content] ? markedFields[content] : []
                    // });
                    measureChips.push(content);
                } else {
                    // console.log("markField[content] ", markedFields[content]);
                    jsonArray.push({
                        "axis": columnName,
                        "dimensionName": content,
                        "dimensionValues": markedFields[content] ? markedFields[content] : [],
                        "rollups": true,
                        "groupby": true
                    })
                }
            })

        });

        // ["SRCost", "SalesAchieved"]
        if (measureChips.length > 0) {
            jsonArray.push(
                { "axis": "Column", "dimensionName": "Measures", "dimensionValue": measureChips  }
            );
        }

        // const dummy = [{ "axis": "row", "dimensionName": "Employee Name", "dimensionValue": [], "rollups": true, "groupby": true }, { "axis": "row", "dimensionName": "Department", "dimensionValue": [], "rollups": true, "groupby": true }, { "axis": "Column", "dimensionName": "Measures", "dimensionValue": ["SRCost", "SalesAchieved"] }]

        // console.log("dummy ",dummy);
        console.log("jsonArray ",jsonArray);



        let finalData = {
            "reportParameters": jsonArray,
            "isGDF": true,
            "isHDF": true,
            "calculatedMeasures": formatApiDataToCalcMeasures(calculatedMeasuers)
        }
        // console.log("jsonArray ", finalData);

        setResultJSON(finalData);
    }, [markedFields, columns])

    return (<div >
        <table className="table">
            <thead style={{ display: 'block' }}>
                <tr>
                    <th></th>
                    <th style={{ width: '100%' }}>{selectedField}</th>
                </tr>
            </thead>
            <tbody style={{
                display: 'block',
                overflow: 'auto',
                height: '7rem',
                width: '100%'
            }}>

                {
                    selectedFieldChips.map((field, index) => {
                        const checkBoxValue = markedFields[selectedField]?.includes(field) ? true : false;
                        return (
                            <tr style={{ display: 'block' }} key={`selected-chip-${index}`}>
                                <td>
                                    <input type='checkbox'
                                        className="fieldCheckBox"
                                        checked={checkBoxValue}
                                        onChange={(e) => {
                                            markField(e.target.checked, field);
                                        }}
                                    /></td>
                                <td style={{ width: '100%', paddingLeft: '0px' }}>{field}</td>
                            </tr>
                        )
                    })
                }

            </tbody>
        </table>
    </div>)
}


export default React.memo(FilterSection);



