// @ts-nocheck
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import SampleTable from "../SampleTable/SampleTable";
import CollapseMenu from "../CollapseMenu/CollapseMenu";
import './AnalysisEngine.css'

import AWS from 'aws-sdk';
import { getColumnsFromBackEnd } from "../../helpers/Analysis/AnalysisHelper";
import { getMeasuresByModel } from "../../store/actions/actions";
AWS.config.update({
    accessKeyId: 'AKIAYGM5KXDRTOXZNUN2',
    secretAccessKey: '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',
    region: 'us-west-2'
})

//unzip
const JSZip = require("jszip");


const AnalysisEngine = (props) => {



    const [columnsFromBackend, setColumns] = useState(null);
    const [parsedZipData, setParsedZipData] = useState(null);

    const [showCollapseMenu, toggleCollapseMenu] = useState(true);

    const dispatch = useDispatch();
    const measuresByModel = useSelector((state: any) => state?.skilletAnalyticsPageData?.measures);

    useEffect(() => {
        dispatch(getMeasuresByModel('102'));
        downloadFileAndConvert();
    }, []);

    useEffect(() => {
        if (parsedZipData && measuresByModel) {
            const convertedData = getColumnsFromBackEnd(parsedZipData, measuresByModel);
            setColumns(convertedData)
        }
    }, [parsedZipData, measuresByModel]);


    const [grid, setGrid] = useState([10, 2]);

    const getAccordian = (accordianLength) => {
        if (accordianLength === 0) {
            setGrid([10, 2]);
        }

        else if (accordianLength === 1) {
            setGrid([9, 3]);
        }

        else if (accordianLength > 1) {
            setGrid([8, 4]);
        }
    }



    const downloadFileAndConvert = () => {

        const s3 = new AWS.S3();
        const testCaseFileName = 'SalesSampleData';

        const getParams = { Bucket: 'sfperftest', Key: testCaseFileName + '.zip' }

        const testCaseJsonFileName = testCaseFileName;

        s3.getObject(getParams, async (err, data) => {
            // Handle any error and exit
            if (err) {
                console.log(err);
                return err;
            }

            const new_zip = new JSZip();



            try {
                const zip = await new_zip.loadAsync(data.Body);
                const rawData = await zip.file(testCaseJsonFileName + '.json').async("string");
                const jsonData = JSON.parse(rawData);
                setParsedZipData(jsonData);
            }
            catch (e) {

            }
        });
    };

    const testFn = () => {
        for (const [i, v] of ['a', 'b', 'c','d','e'].entries()) {
            if(i === 3) {
                break;
            }
            console.log(i, v);
          }
          
    }


    return (<>
        {/* <h1>Analysis Engine UI</h1> */}

        <div style={{ display: 'flex', alignItems: 'center' }}>
            <div>
                <button className="btn" onClick={testFn}>Refresh</button>
            </div>
            <div>
                <button className="btn">Expand</button>
            </div>
            <div>
                <button className="btn">Collapse</button>
            </div>
            <div>
                <button className="btn">Refresh</button>
            </div>
            <div>
                <input type="checkbox" onChange={() => { }} /> Row Headers
            </div>
        </div>
        <div className="row analysisUIMainDiv">

            <div>
                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                    {/* <div style={{ display: 'flex', alignItems: 'center' }}>
                        <h1>Key Metrics of Sales Performance Model</h1>
                        <span className="editModeChip">Edit Mode</span>
                        <button className="btn" onClick={() => { toggleCollapseMenu(!showCollapseMenu) }}>Field List</button>
                    </div> */}

                    <div className="sql-frame-analysis-button-div" style={{ display: 'flex' }}>
                        {/* <button className="btn">UndoIcon</button>
                        <button className="btn">RedoIcon</button>
                        <button className="btn">InfoOutlinedIcon</button>
                        <button className="btn">ShareIcon</button>
                        <button className="btn btn-dark" style={{ 'borderRadius': '8px'}}>
                            <span>Save</span><span>KeyboardArrowDownIcon</span>
                        </button> */}



                    </div>
                </div>


                <div className={`mt-3 ml-3 sf ${(grid[0] == 9 || grid[0] == 10) ? 'analysis-sql-frame-max-width' : 'analysis-sql-frame-div'}`} id="DynamicReport"></div>
            </div>
            {
                columnsFromBackend ?
                    <div className="collapseMenuMainDiv" style={{ display: showCollapseMenu ? 'block' : 'none' }}>
                        <CollapseMenu setAccordian={getAccordian} columnsFromBackend={columnsFromBackend} />
                    </div>
                    :
                    <h1>Loading ....</h1>

            }
        </div>


    </>)
}

export default React.memo(AnalysisEngine);





