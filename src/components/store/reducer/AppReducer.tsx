import { calculatedMeasuers } from "../../MyTable";


const initialState = {};
const AppReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_MEASURES_BY_MODEL':
            return { ...state, ...{ skilletAnalyticsPageData: { measures: calculatedMeasuers } } }
        default:
            return state
    }
}

export default AppReducer;