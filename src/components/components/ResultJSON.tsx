import React from "react";

export function ResultJSON({ resultJSON }) {

    return (<div>
        <h1>Result JSON</h1>
        <div style={{ backgroundColor: 'lightgrey' }}>{JSON.stringify(resultJSON)}</div>
    </div>);

}