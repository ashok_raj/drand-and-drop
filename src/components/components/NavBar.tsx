// @ts-nocheck

import React from "react";
import { Link } from "react-router-dom"
import '../css/NavBar.css';


const NavBar = () => {
    return (
        <nav className="navbar navbar-expand-sm bg-dark container mb-3">

            <ul className="navbar-nav">
                <li className="nav-item link">
                    <Link to="/main">SQL Frame</Link> |{" "}
                </li>
                <li className="nav-item link">
                    <Link to="/spreadjs">Spread JS</Link> |{" "}
                </li>
                <li className="nav-item link">
                    <Link to="/collapse-menu">Collapse Menu</Link> |{" "}
                </li>
            </ul>

        </nav>
    )
}

export default NavBar;