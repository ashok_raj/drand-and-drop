
// @ts-nocheck

import React, { Component } from "react";

import { SpreadSheets } from '@grapecity/spread-sheets-react';
import GC from "@grapecity/spread-sheets";

class SpreadSheet extends Component {
    hostStyle;

    constructor(props) {
        super(props);
        this.hostStyle = {
            width: '100%',
            height: '600px'
        };
    }

    initSpread(spread) {
        var sheet = spread.getSheet(0);
        var person = { name: 'Peter Winston', age: 25, gender: 'Male', address: { postcode: '10001' } };
        var source = new GC.Spread.Sheets.Bindings.CellBindingSource(person);
        sheet.setBindingPath(2, 2, 'name');
        sheet.setBindingPath(3, 2, 'age');
        sheet.setBindingPath(4, 2, 'gender');
        sheet.setBindingPath(5, 2, 'address.postcode');
        sheet.setDataSource(source);
     
    }

    render() {
        return (
            <div >
                <SpreadSheets hostStyle={this.hostStyle} workbookInitialized={spread => this.initSpread(spread)}>
                </SpreadSheets>

            </div>
        );
    }
}

export default SpreadSheet;

