import React, { useEffect, useState } from "react";
import './css/MyTable.css';
 
export const calculatedMeasuers = [
    {
        "index": 1,
        "func": "sum",
        "col": "Total Actual CTC",
        "as": "SRCost"
    },
    {
        "index": 2,
        "func": "sum",
        "col": "Actual_Sales",
        "as": "SalesAchieved"
    },
    {
        "index": 3,
        "func": "sum",
        "col": "Sales_Target",
        "as": "SalesTarget"
    },
    {
        "index": 4,
        "func": "sum",
        "col": "Lost Count",
        "as": "LostCount"
    },
    {
        "index": 4,
        "func": "sum",
        "col": "Won Count",
        "as": "WonCount"
    },
    {
        "index": 5,
        "func": "sum",
        "col": "Days to Close",
        "as": "DC"
    },
    {
        "index": 6,
        "func": "sum",
        "col": "",
        "expr": "([Won Count]+[Lost Count])",
        "as": "DealCount"
    },
    {
        "index": 7,
        "func": "",
        "col": "",
        "expr": '([WonCount]/[DealCount])*100',
        "as": "DealConversionRate"
    },
    {
        "index": 8,
        "func": "",
        "col": "",
        "expr": '([SalesAchieved]/[WonCount])',
        "as": "AvgOrdVal-WonOnly",
        "format": {
            "type": "NumberFormat",
            "locale": "en-US",
            "options": {
                "maximumFractionDigits": 2
            }
        }
    },
    {
        "index": 9,
        "func": "",
        "col": "",
        "expr": '([DC]/([DealCount]))',
        "as": "AverageTimeToClose(days)"
    },
    {
        "index": 10,
        "func": "",
        "col": "",
        "expr": '([SRCost]/[SalesAchieved])*100',
        "as": "SRCost vs SalesAchieved",
        "format": {
            "type": "NumberFormat",
            "locale": "en-US",
            "options": {
                "style": "percent",
                "minimumFractionDigits": 1
            }
        }
    },
    {
        "index": 11,
        "func": "",
        "col": "",
        "expr": '([SalesAchieved]/[SalesTarget])*100',
        "as": "Sales Achivement",
        "format": {
            "type": "NumberFormat",
            "locale": "en-US",
            "options": {
                "maximumFractionDigits": 2
            }
        }
    },
    {
        "index": 12,
        "func": "ranko",
        "partitionBy": [],
        "orderBy": {
            "field": "AverageTimeToClose(days)",
            "order": "ASC"
        },
        "as": "RankTimeToCloseDays"
    },
    {
        "index": 13,
        "func": "",
        "col": "",
        "expr": '([SRCost vs SalesAchieved]*0.15)+([Sales Achivement]*0.5)+([AvgOrdVal-WonOnly]*0.1)+([DealConversionRate]*0.1)+([AverageTimeToClose(days)]*0.15)',
        "as": "CalcScore",
        "format": {
            "type": "NumberFormat",
            "locale": "en-US",
            "options": {
                "maximumFractionDigits": 2
            }
        }
    },
    {
        "index": 14,
        "func": "ranko",
        "partitionBy": [],
        "orderBy": {
            "field": "CalcScore",
            "order": "DESC"
        },
        "as": "Over All Rank"
    },
    {
        "index": 15,
        "func": "ranko",
        "partitionBy": [],
        "orderBy": {
            "field": "SRCost vs SalesAchieved",
            "order": "DESC"
        },
        "as": "Cal_Rank_E"
    },
    // {
    //     "index": 16,
    //     "func": "ranko",
    //     "partitionBy": ["Department"],
    //     "orderBy": {
    //         "field": "CalcScore",
    //         "order": "DESC"
    //     },
    //     "as": "Rank within Region"
    // }
]
const MyTable = ({ selectedField, selectedFieldChips, columns, setResultJSON }) => {
 
 
    console.log("columns ", columns);
 
    const [markedFields, setMarkedFields] = useState({});
 
 
    const markField = (selected, fieldName) => {
 
        // console.log("markedFields ", markedFields)
        console.log('selected',selected);
        console.log('selectedField', selectedField);
        console.log('fieldName',fieldName);

        const copy = { ...markedFields };
 
        if (selected) {
            if (!copy[selectedField]) {
                copy[selectedField] = [];
            }
 
            copy[selectedField].push(fieldName);
 
        } else {
            const fieldIndex = copy[selectedField].indexOf(fieldName);
            copy[selectedField].splice(fieldIndex, 1);
        }
        setMarkedFields(copy);
    }
 
 
    useEffect(() => {
        let jsonArray: any = [];
 
        ['row', 'column', 'values', 'page'].forEach((columnName) => {
 
            columns[columnName].items.forEach(({ content }) => {
 
                if (columnName === 'values' && content == "Measures") {
                    jsonArray.push({
                        "axis": "Column",
                        "dimensionName": "Measures",
                        "dimensionValues": markedFields[content] ? markedFields[content] : []
                    });
                } else {
                    // console.log("markField[content] ", markedFields[content]);
                    jsonArray.push({
                        "axis": columnName,
                        "dimensionName": content,
                        "dimensionValues": markedFields[content] ? markedFields[content] : [],
                        "rollups": true,
                        "groupby": true
                    })
                }
            })
 
        });
 
 
        let finalData = {
            "reportParameters": jsonArray,
            "isGDF": true,
            "isHDF": true,
            "calculatedMeasures": calculatedMeasuers
        }
        console.log("jsonArray ", finalData);
 
        setResultJSON(finalData);
    }, [markedFields, columns])
 
    return (<div >
        <table className="table">
            <thead style={{ display: 'block' }}>
                <tr>
                    <th></th>
                    <th style={{ width: '100%' }}>{selectedField}</th>
                </tr>
            </thead>
            <tbody style={{
                display: 'block',
                overflow: 'auto',
                height: '200px',
                width: '100%'
            }}>
 
                {
                    selectedFieldChips.map((field, index) => {
                        const checkBoxValue = markedFields[selectedField]?.includes(field) ? true : false;
                        return (
                            <tr style={{ display: 'block' }} key={`selected-chip-${index}`}>
                                <td>
                                    <input type='checkbox'
                                        className="fieldCheckBox"
                                        checked={checkBoxValue}
                                        onChange={(e) => {
                                            markField(e.target.checked, field);
                                        }}
                                    /></td>
                                <td style={{ width: '100%' }}>{field}</td>
                            </tr>
                        )
                    })
                }
 
            </tbody>
        </table>
    </div>)
}
 
 
export default React.memo(MyTable);
 
 

