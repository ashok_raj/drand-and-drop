import { sqlframes } from '@sqlframes/repl-app/react';

import '@sqlframes/repl-app/styles';

const { DataFrame, View, SQL } = sqlframes;
const {
    groupBy, rollup, partitionBy, orderBy,
    where: { feq, eq, and, gte, gt, lte, lt, or, isin }
    // agg: { rank, ranko, avg, lead, percentRank }
} = SQL;


const queryGeneration = async (jsonArrData, reportDef) => {

    let dfHeadersArr = [];
    let dfDimensionFieldsArr = [];
    let dfMeasuresFieldsArr = [];

    let dfInput = DataFrame.fromJSONArray(jsonArrData);

    let finalDf = dfInput;

    // 1) FDF  using Page, and dimension values
    let arrFilters = [];
    if (reportDef.reportParameters.length > 0) {
        for (let i = 0; i < reportDef.reportParameters.length; i++) {
            let filter;

            try {
                if (reportDef.reportParameters[i].dimensionValue == null) {
                    reportDef.reportParameters[i].dimensionValue = '[]';
                }
                reportDef.reportParameters[i].dimensionValue = JSON.parse(reportDef.reportParameters[i].dimensionValue);
            } catch (e) { }

            if (reportDef.reportParameters[i].dimensionName == "Measures") {
                dfHeadersArr.push(...reportDef.reportParameters[i].dimensionValue);
                dfMeasuresFieldsArr.push(...reportDef.reportParameters[i].dimensionValue);
                continue;
            }

            dfDimensionFieldsArr.push(reportDef.reportParameters[i].dimensionName);
            dfHeadersArr.push(reportDef.reportParameters[i].dimensionName);

            if (reportDef.reportParameters[i].dimensionValue.length > 0) {
                filter = isin(reportDef.reportParameters[i].dimensionName, reportDef.reportParameters[i].dimensionValue);
            }
            if (filter != null) {
                arrFilters.push(filter);
            }
        }
    }

    console.log("dfHeadersArr", dfHeadersArr);
    console.log("dfDimensionFieldsArr", dfDimensionFieldsArr);

    const filteredFinaldf = finalDf.fdf(
        and(
            ...arrFilters
        ),
    );

    // 2) Measures --> Expr fields, Agg fields, rank fields
    let dynamicAggAndCalcFields = [];
    if (reportDef.calculatedMeasures.length > 0) {
        for (let i = 0; i < reportDef.calculatedMeasures.length; i++) {

            // 1. Construct the calculated field (expr or agg func field)
            let aggOrCalcField;
            //Expr case
            if (reportDef.calculatedMeasures[i].expr != null && reportDef.calculatedMeasures[i].expr != "") {
                // expr used with some agg func
                if (reportDef.calculatedMeasures[i].func != null && reportDef.calculatedMeasures[i].func != "") {
                    let sqlScript = SQL.script(reportDef.calculatedMeasures[i].expr);
                    let paramsArr = buildParamsListForMeasure(reportDef.calculatedMeasures[i].func_params);
                    let exprFieldFuncParamsArr = [sqlScript, ...paramsArr];
                    //agg func with expr
                    aggOrCalcField = SQL.agg[reportDef.calculatedMeasures[i].func](...exprFieldFuncParamsArr).as(reportDef.calculatedMeasures[i].as);
                } else {
                    //just expr
                    aggOrCalcField = SQL.script(reportDef.calculatedMeasures[i].expr).as(reportDef.calculatedMeasures[i].as);
                }
            } else {    //agg functions like sum, rank, ranko even with multiple params
                let funcParamsArr = buildParamsListForMeasure(reportDef.calculatedMeasures[i].func_params);
                //agg func with col/field
                // console.log("func ",i, reportDef.calculatedMeasures[i].func)
                try {
                    aggOrCalcField = SQL.agg[reportDef.calculatedMeasures[i].func](...funcParamsArr)
                    .as(reportDef.calculatedMeasures[i].as);
                } catch(e) {
                    // console.log("Error func ",i, reportDef.calculatedMeasures[i].func)
                }
            }

            // 2. Push the agg or calc field to list of measures
            if (aggOrCalcField != null) {
                dynamicAggAndCalcFields.push(aggOrCalcField);
            }
        }
    }

    // 3) GDF
    let groupByRowAxisArr = [];
    let groupByColAxisArr = [];
    let rollupsRowAxisArr = [];
    let rollupsColAxisArr = [];
    for (let i = 0; i < reportDef.reportParameters.length; i++) {
        if (reportDef.reportParameters[i].dimensionName != "Measures") {
            if (reportDef.reportParameters[i].axis == "row") {
                if (reportDef.reportParameters[i].groupby == true && reportDef.reportParameters[i].rollups == false) {
                    groupByRowAxisArr.push(reportDef.reportParameters[i].dimensionName);
                } else if (reportDef.reportParameters[i].groupby == true && reportDef.reportParameters[i].rollups == true) {
                    rollupsRowAxisArr.push(reportDef.reportParameters[i].dimensionName);
                }
            } else if (reportDef.reportParameters[i].axis == "column") {
                if (reportDef.reportParameters[i].groupby == true && reportDef.reportParameters[i].rollups == false) {
                    groupByColAxisArr.push(reportDef.reportParameters[i].dimensionName);
                } else if (reportDef.reportParameters[i].groupby == true && reportDef.reportParameters[i].rollups == true) {
                    rollupsColAxisArr.push(reportDef.reportParameters[i].dimensionName);
                }
            }
        }
    }
    let finalGdf;
    if (reportDef.isGDF) {
        // GDF
        finalGdf = filteredFinaldf.gdf(
            groupBy(
                //...reportDef.grouping.groupBy.cols,    // can also be groupby([cols], [cols], [rollups], [rollups])
                ...groupByRowAxisArr, ...groupByColAxisArr,
                rollup(...rollupsRowAxisArr), rollup(...rollupsColAxisArr)
            ), //make dynamic for any no of rollups and any no of combination of cols
            ...dynamicAggAndCalcFields,         //agg
        )
    }




    // 4) Formatting...
    for (let indexMeasures = 0; indexMeasures < reportDef.calculatedMeasures.length; ++indexMeasures) {
        if (reportDef.calculatedMeasures[indexMeasures].format == null) {
            const defaultFormatter = new Intl.NumberFormat('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
            finalGdf.fmt.setFieldFormatter(reportDef.calculatedMeasures[indexMeasures].as, defaultFormatter);
            continue;
        }
        switch (reportDef.calculatedMeasures[indexMeasures].format.type) {
            case "NumberFormat":
                let nbrFormatter = new Intl.NumberFormat(reportDef.calculatedMeasures[indexMeasures].format.locale, reportDef.calculatedMeasures[indexMeasures].format.options);
                finalGdf.fmt.setFieldFormatter(reportDef.calculatedMeasures[indexMeasures].as, nbrFormatter);
                break;
        }
    }

    // 5) pdf
    let pdfCols = [];
    for (let i = 0; i < reportDef.reportParameters.length; i++) {
        if (reportDef.reportParameters[i].dimensionName == "Measures") {
            pdfCols = reportDef.reportParameters[i].dimensionValue;
        }
    }
    let finalPdf = finalGdf.pdf(...pdfCols);


    // 6) VDF
    // rows axis and cols axis
    let finalVdf = finalPdf;
    // if(reportDef.isVDF == true) {
    finalVdf = finalPdf.vdf(rollupsRowAxisArr, rollupsColAxisArr);
    // }

    //TODO: it is only applying to column axis field... if applied to GDF then only applying to row axis fields...
    // 9) apply css styles
    // Apply styles to headers...
    const style = { color: 'black', 'font-size': '14px', 'background-color': '#fafafa' };
    for (let indexHeaderArr = 0; indexHeaderArr < dfHeadersArr.length; indexHeaderArr++) {
        finalVdf.fmt.addFieldHeaderStyler(dfHeadersArr[indexHeaderArr], finalVdf.fmt.const(style));
    }
    //Apply styles for dimension fields...
    const dimFieldsStyle = { color: 'black', 'font-size': '14px', 'font-weight': 'normal', 'background-color': '#fafafa' };
    for (let indexDimArr = 0; indexDimArr < dfDimensionFieldsArr.length; indexDimArr++) {
        finalVdf.fmt.addFieldStyler(dfDimensionFieldsArr[indexDimArr], finalVdf.fmt.const(dimFieldsStyle));
    }
    //Apply styles to measure fields...
    const measureFieldsStyle = { color: 'black', 'font-size': '14px', 'font-weight': 'normal' };
    for (let indexMeasureArr = 0; indexMeasureArr < dfMeasuresFieldsArr.length; indexMeasureArr++) {
        finalVdf.fmt.addFieldStyler(dfMeasuresFieldsArr[indexMeasureArr], finalVdf.fmt.const(measureFieldsStyle));
    }
    // apply styles to data rows...
    finalVdf.fmt.addRowStyler(finalVdf.fmt.const({ color: 'black', 'font-size': '14px', 'font-weight': 'normal', 'background-color': 'white' }));


    // 7) HDF
    let finalHdf = finalVdf;
    if (reportDef.isHDF == true) {
        finalHdf = finalVdf.hdf(); //finalPdf.hdf(...rollupsArr);
        finalHdf.hideRoot = true;
        finalHdf.singleHierarchyField = false
        // finalHdf.expandLevels = 10;
    }

    // console.log("========================== DATA FRAME - GETDATA() ===================================");
    // var dfDataResponse = await finalHdf.getData();
    // console.log("===================== DF_HEADERS ========================");
    // console.log(dfDataResponse.headers);
    // console.log("===================== DF_DATA ========================");
    // console.log(dfDataResponse.data);
    // console.log(dfData);

    // 8) Hide left menu bars
    finalHdf.ui.showMenubarLeft = false;

    //Change [All] to blank for summary rows...
    DataFrame.Config.SummaryName = '';

    //Remove default SQL frame styles...
    finalHdf.ui.colorCoded = false;

    // // 9) apply css styles
    // // Apply styles to headers...
    // const style = { color: 'black', 'font-size':'14px', 'background-color': '#fafafa' };
    // for(let indexHeaderArr = 0; indexHeaderArr < dfHeadersArr.length; indexHeaderArr++) {
    //  finalHdf.fmt.addFieldHeaderStyler(dfHeadersArr[indexHeaderArr], finalHdf.fmt.const(style));
    // }
    // //Apply styles for dimension fields...
    // const dimFieldsStyle = { color: 'black', 'font-size':'14px', 'font-weight': 'normal', 'background-color': '#fafafa' };
    // for(let indexDimArr = 0; indexDimArr < dfDimensionFieldsArr.length; indexDimArr++) {
    //  finalHdf.fmt.addFieldStyler(dfDimensionFieldsArr[indexDimArr], finalHdf.fmt.const(dimFieldsStyle));
    // }
    // //Apply styles to measure fields...
    // const measureFieldsStyle = { color: 'black', 'font-size': '14px', 'font-weight': 'normal'};
    // for(let indexMeasureArr = 0; indexMeasureArr < dfMeasuresFieldsArr.length; indexMeasureArr++) {
    //  finalHdf.fmt.addFieldStyler(dfMeasuresFieldsArr[indexMeasureArr], finalHdf.fmt.const(measureFieldsStyle));
    // }
    // // apply styles to data rows...
    // finalHdf.fmt.addRowStyler(finalHdf.fmt.const({ color: 'black', 'font-size':'14px', 'font-weight': 'normal', 'background-color': 'white' }));


    // 10) Render
    if (dynamicAggAndCalcFields.length > 0 && dfDimensionFieldsArr.length > 0) {
        View.render(document.getElementById('DynamicReport'), finalHdf);
    } else {
        View.render(document.getElementById('DynamicReport'), null);
    }
}

const buildParamsListForMeasure = (funcParams) => {
    // function getParamsList(funcParams) {
    if (!funcParams) {
        funcParams = [];
    }
    let funcParamsArr = [];
    for (let indexParam = 0; indexParam < funcParams.length; indexParam++) {
        //When param is itself another function like orderby/partitionby then it will have again list of arguments
        //Here param is func(list of params) eg: ranko() will take two arguments i.e., two functions partitionBy() and orderBy() as paramaters
        if (funcParams[indexParam].isSql != null && funcParams[indexParam].isSql == true) {
            let funcParam;
            if (funcParams[indexParam].func != null) {
                const funcName = funcParams[indexParam].func;
                const params = funcParams[indexParam].func_params;
                funcParam = SQL[funcName](...params);
                funcParamsArr.push(funcParam);
            } else {
                const params = funcParams[indexParam].func_params;
                funcParamsArr.push(...params);
            }
        }
        else {
            //if param is straightforward value or field/col name
            //most of the cases, function having only one param i.e., field name or col name;  just get first param val from array i.e., col/field name
            let obj = funcParams[indexParam];
            let paramVal = obj[Object.keys(obj)[0]];
            funcParamsArr.push(paramVal);
        }
    }
    return funcParamsArr;
}

export default queryGeneration;


