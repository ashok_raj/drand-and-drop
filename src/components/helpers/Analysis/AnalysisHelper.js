// import { jsonData } from "./assets/jsonData";

import uuid from 'uuid/v4';
// import { jsonData } from "./assets/jsonData";

export const calculatedMeasuers1 = [
    {
        "index": 1,
        "func": "sum",
        "col": "Total Actual CTC",
        "as": "SRCost"
    },
    {
        "index": 2,
        "func": "sum",
        "col": "Actual_Sales",
        "as": "SalesAchieved"
    },
    {
        "index": 3,
        "func": "sum",
        "col": "Sales_Target",
        "as": "SalesTarget"
    },
    {
        "index": 4,
        "func": "sum",
        "col": "Lost Count",
        "as": "LostCount"
    },
    {
        "index": 4,
        "func": "sum",
        "col": "Won Count",
        "as": "WonCount"
    },
    {
        "index": 5,
        "func": "sum",
        "col": "Days to Close",
        "as": "DC"
    },
    {
        "index": 6,
        "func": "sum",
        "col": "",
        "expr": "([Won Count]+[Lost Count])",
        "as": "DealCount"
    },
    {
        "index": 7,
        "func": "",
        "col": "",
        "expr": '([WonCount]/[DealCount])*100',
        "as": "DealConversionRate"
    },
    {
        "index": 8,
        "func": "",
        "col": "",
        "expr": '([SalesAchieved]/[WonCount])',
        "as": "AvgOrdVal-WonOnly",
        "format": {
            "type": "NumberFormat",
            "locale": "en-US",
            "options": {
                "maximumFractionDigits": 2
            }
        }
    },
    {
        "index": 9,
        "func": "",
        "col": "",
        "expr": '([DC]/([DealCount]))',
        "as": "AverageTimeToClose(days)"
    },
    {
        "index": 10,
        "func": "",
        "col": "",
        "expr": '([SRCost]/[SalesAchieved])*100',
        "as": "SRCost vs SalesAchieved",
        "format": {
            "type": "NumberFormat",
            "locale": "en-US",
            "options": {
                "style": "percent",
                "minimumFractionDigits": 1
            }
        }
    },
    {
        "index": 11,
        "func": "",
        "col": "",
        "expr": '([SalesAchieved]/[SalesTarget])*100',
        "as": "Sales Achivement",
        "format": {
            "type": "NumberFormat",
            "locale": "en-US",
            "options": {
                "maximumFractionDigits": 2
            }
        }
    },
    {
        "index": 12,
        "func": "ranko",
        "partitionBy": [],
        "orderBy": {
            "field": "AverageTimeToClose(days)",
            "order": "ASC"
        },
        "as": "RankTimeToCloseDays"
    },
    {
        "index": 13,
        "func": "",
        "col": "",
        "expr": '([SRCost vs SalesAchieved]*0.15)+([Sales Achivement]*0.5)+([AvgOrdVal-WonOnly]*0.1)+([DealConversionRate]*0.1)+([AverageTimeToClose(days)]*0.15)',
        "as": "CalcScore",
        "format": {
            "type": "NumberFormat",
            "locale": "en-US",
            "options": {
                "maximumFractionDigits": 2
            }
        }
    },
    {
        "index": 14,
        "func": "ranko",
        "partitionBy": [],
        "orderBy": {
            "field": "CalcScore",
            "order": "DESC"
        },
        "as": "Over All Rank"
    },
    {
        "index": 15,
        "func": "ranko",
        "partitionBy": [],
        "orderBy": {
            "field": "SRCost vs SalesAchieved",
            "order": "DESC"
        },
        "as": "Cal_Rank_E"
    },
    // {
    //     "index": 16,
    //     "func": "ranko",
    //     "partitionBy": ["Department"],
    //     "orderBy": {
    //         "field": "CalcScore",
    //         "order": "DESC"
    //     },
    //     "as": "Rank within Region"
    // }
]

let dataObj;
let itemsFromBackend;
let itemsFromBackendCopy;
let formattedArr;

export const getDataObj = () => {
    return dataObj;
}


export const getItemsFromBackend = () => {
    return itemsFromBackendCopy;
}

export const getFormattedArr = () => {
    return formattedArr;
}



export const getColumnsFromBackEnd = (jsonData, calculatedMeasuers) => {

    itemsFromBackend = jsonData.fields.map((data) => {
        return { id: uuid(), content: data, source: 'fields' };
    });

    // console.log("itemsFromBackend ", itemsFromBackend);

    const measuresIndex = itemsFromBackend.push({ id: uuid(), content: "Measures" });
    formattedArr = [];
    const rowData = jsonData.data;
    const header = jsonData.fields;
    rowData.forEach((row) => {
        let obj = {};
        row.forEach((element, index) => {
            obj[header[index]] = element;
        })
        formattedArr.push(obj)
    })



    // console.log("formattedArr ", formattedArr)



    dataObj = {};

    itemsFromBackend.forEach((it, i) => {
        dataObj[i] = [];
    })

    jsonData.data.map((dataArr) => {
        dataArr.forEach((d, i) => {
            if (!dataObj[i].includes(d))
                dataObj[i].push(d);
        })
    });


    calculatedMeasuers.forEach((calcObj) => {
        dataObj[measuresIndex - 1].push(calcObj.as)
    })


    const measuresFromBackend = calculatedMeasuers.map((caclObj) => {
        const measureName = caclObj.col ? caclObj.col : caclObj.as;
        return { id: uuid(), content: measureName, source: 'measures' };
    })
    // console.log("dataObj ", dataObj);

    itemsFromBackendCopy = JSON.parse(JSON.stringify(itemsFromBackend));
    const department = JSON.parse(JSON.stringify(itemsFromBackend[4]));
    // itemsFromBackend.splice(4, 1);


    console.log('department ',department);

    department.id = uuid();

    return {
        ["fields"]: {
            name: "Fields",
            items: itemsFromBackend
        },
        ["row"]: {
            name: "Row",
            items: [department]
        },
        ["column"]: {
            name: "Column",
            items: []
        },
        ["values"]: {
            name: "Values",
            items: []
        },
        ["measures"]: {
            name: "measures",
            items: measuresFromBackend
        }
    };





}






