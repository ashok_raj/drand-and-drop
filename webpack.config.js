const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

// Instantiate the plugin.
// The `template` property defines the source
// of a template file that this plugin will use.
// We will create it later.
const htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html",
});

module.exports = {
  // Our application entry point.
  entry: "./src/index.tsx",
  
  externals: {
    'node-fetch': { amd: 'node-fetch', global: 'fetch' },
    'requirejs': 'requirejs',
    'dompurify': { amd: 'dompurify', global: 'DOMPurify' },
    // 'date-fns': { amd: 'date-fns', global: 'dateFns' }, // example of pre-bundling date-fns
    'papaparse': 'papaparse',
    // 'monaco-editor': { amd: 'monaco-editor', global: 'monaco' },
    'htm': 'htm',
    'preact': 'preact',
    '@yaireo/tagify': {amd: '@yaireo/tagify', global: 'Tagify' },
    'interactjs': { amd: 'interactjs', global: 'interact' },
    'echarts': 'echarts',
    'acorn': 'acorn',
    'eta': { amd: 'eta', global: 'Eta' },
  },

  // These rules define how to deal 
  // with files with given extensions.
  // For example, .tsx files 
  // will be compiled with ts-loader,
  // a spcific loader for webpack
  // that knows how to work with TypeScript files.
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      }
    ],
  },

  // Telling webpack which extensions
  // we are interested in.
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },

  // What file name should be used for the result file,
  // and where it should be palced.
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist"),
    library: {
      type: 'amd-require'
    }
  },

  // Use the html plugin.
  plugins: [
    new CopyWebpackPlugin(
        { 
          patterns: [{
            from: 'node_modules/@sqlframes/repl-app/dist/libs.mjs', to: 'js/sqlframes/libs.mjs' 
          },{
            from: 'node_modules/@sqlframes/repl-app/dist/main.mjs', to: 'js/sqlframes/main.mjs' 
          },
          ]
        }),
  
      new CopyWebpackPlugin(
        { 
          patterns: [{
            from: 'node_modules/@sqlframes/repl-app/dist/api.d.ts', to: 'api/api.d.ts' },
          ]
        }),
  
      new CopyWebpackPlugin(
        { 
          patterns: [{
            from: 'node_modules/@sqlframes/repl-app/dist/styles/themes/*', to: 'styles/themes/[name][ext]' },
          ]
        }),
      htmlPlugin,
    ],

  // Set up the directory 
  // from which webpack will take the static content.
  // The port field defines which port on localhost
  // this application will take.
  devServer: {
    // static: './',
    // contentBase: path.join(__dirname, 'dist'),
    static: {
        directory: path.join(__dirname, "public")
      },
    compress: true,
    port: 9006
  }
};